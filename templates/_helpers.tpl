{{/*
Expand the name of the chart.
*/}}
{{- define "wiedoetermee.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "wiedoetermee.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create unified labels for wiedoetermee components
*/}}
{{- define "wiedoetermee.common.matchLabels" -}}
app: {{ template "wiedoetermee.name" . }}
release: {{ .Release.Name }}
{{- end -}}

{{- define "wiedoetermee.common.metaLabels" -}}
chart: {{ template "wiedoetermee.chart" . }}
heritage: {{ .Release.Service }}
{{- end -}}

{{- define "wiedoetermee.wdm.labels" -}}
{{ include "wiedoetermee.wdm.matchLabels" . }}
{{ include "wiedoetermee.common.metaLabels" . }}
{{- end -}}

{{- define "wiedoetermee.wdm.matchLabels" -}}
component: {{ .Values.wdm.name | quote }}
{{ include "wiedoetermee.common.matchLabels" . }}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "wiedoetermee.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create a fully qualified wdm name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}

{{- define "wiedoetermee.wdm.fullname" -}}
{{- if .Values.wdm.fullnameOverride -}}
{{- .Values.wdm.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.wdm.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.wdm.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Return the appropriate apiVersion for deployment.
*/}}
{{- define "deployment.apiVersion" -}}
{{- print "apps/v1" -}}
{{- end -}}
{{/*

{{/*
Return the appropriate apiVersion for ingress.
*/}}
{{- define "ingress.apiVersion" -}}
{{- print "networking.k8s.io/v1" -}}
{{- end -}}

{{/*
Define the wiedoetermee.namespace template if set with forceNamespace or .Release.Namespace is set
*/}}
{{- define "wiedoetermee.namespace" -}}
{{- if .Values.forceNamespace -}}
{{ printf "namespace: %s" .Values.forceNamespace }}
{{- else -}}
{{ printf "namespace: %s" .Release.Namespace }}
{{- end -}}
{{- end -}}

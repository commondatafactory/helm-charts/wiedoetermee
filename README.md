# Grip Op Informatie (GOI) Helm Chart

This functionality is in development and is subject to change. The code is provided as-is with no warranties.

## Requirements

This application needs to be installed according to the Dutch municipal standard [Haven](https://haven.commonground.nl/)
for deploying cloud applications.

In addition, [Helm](https://helm.sh) must be installed to use this chart.

Please refer to Helm's [documentation](https://helm.sh/docs/) to get started.

## Installing the wiedoetermee application

Once Helm is set up properly, install the Helm chart as follows:

First, get a local copy by cloning the Helm chart `git clone https://gitlab.com/commondatafactory/helm-charts/wiedoetermee.git wiedoetermee`

Next, please inspect `values.yaml` for possible configuration options, make the desired changes where needed.

Finally, run the following command:

```console
helm install -f ./wiedoetermee/values.yaml wdm ./wiedoetermee
```

Optionally, you can then run `helm test wdm` to test the deployment(s).

Note: when applicable add `-n <namespace>` to run the commands in namespace.
